Data-Model:


- User-Entity:
	-	id
	- 	prename
	-	surename
	-	email
	-	phone-number
	-	created_date
	-	changed-date
	-	userCredentialsId	(One-To-One)
	-	collection<long> ids	(One-To-Many)


- User-Credentials
	-	id
	-	username
	-	pwd (crypted)
	-	created_date
	-	changed-date
	- 	user-id (One-To-One)

- Address
	-	id
	-	Street
	-	Number
	-	Zip
	-	City
	-	Country
	-	created_date
	-	changed-date
	-	userId (Many-to-One Relationship)
	