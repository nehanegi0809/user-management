package com.tech11.UserManagement.data.models;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Objects;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Table(name = "tech11users")
public class UserEntity {
	/**
	 * Meta-Data Begin
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotNull
	private LocalDateTime createdDate;

	@NotNull
	private LocalDateTime changedDate;

	/**
	 * Meta-Data End
	 */
	@NotNull
	private String prename;
	@NotNull
	private String surename;
	@NotNull
	private String email;
	@NotNull
	private String phone;

	@OneToOne(targetEntity = CredentialsEntity.class)
	private long userCredentialsId;

	@OneToMany(targetEntity = AddressEntity.class)
	private Collection<Long> ids;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public LocalDateTime getChangedDate() {
		return changedDate;
	}

	public void setChangedDate(LocalDateTime changedDate) {
		this.changedDate = changedDate;
	}

	public String getPrename() {
		return prename;
	}

	public void setPrename(String prename) {
		this.prename = prename;
	}

	public String getSurename() {
		return surename;
	}

	public void setSurename(String surename) {
		this.surename = surename;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public long getUserCredentialsId() {
		return userCredentialsId;
	}

	public void setUserCredentialsId(long userCredentialsId) {
		this.userCredentialsId = userCredentialsId;
	}

	public Collection<Long> getIds() {
		return ids;
	}

	public void setIds(Collection<Long> ids) {
		this.ids = ids;
	}

	@Override
	public int hashCode() {
		return Objects.hash(changedDate, createdDate, email, ids, phone, prename, surename);
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = false;

		if (obj != null) {
			if (obj instanceof UserEntity) {
				UserEntity otherEntity = (UserEntity) obj;
				result = otherEntity.hashCode() == hashCode();
			}
		}

		return result;
	}

	@Override
	public String toString() {
		return "UserEntity [id=" + id + ", createdDate=" + createdDate + ", changedDate=" + changedDate + ", prename="
				+ prename + ", surename=" + surename + ", email=" + email + ", phone=" + phone + ", userCredentialsId="
				+ userCredentialsId + ", ids=" + ids + "]";
	}

}