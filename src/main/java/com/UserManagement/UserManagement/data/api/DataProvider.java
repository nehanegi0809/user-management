package com.tech11.UserManagement.data.api;

import java.sql.SQLDataException;

import com.tech11.UserManagement.data.models.UserEntity;

public interface DataProvider<Entity> {

	Entity createOrUpdate(UserEntity userEntity) throws Exception;

	Entity read(long id) throws ArgumentException;

	Entity update(UserEntity user) throws ArgumentException;

	boolean delete(long id) throws Exception;

	UserEntity create(UserEntity user) throws SQLDataException;

	boolean delete(UserEntity userEntity) throws ArgumentException;
}
