package com.tech11.UserManagement.data.api;

import java.sql.SQLDataException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import com.tech11.UserManagement.data.models.UserEntity;

@Transactional
public class UserDataProvider implements DataProvider<UserEntity> {

	@PersistenceContext(name = "h2ds")
	private EntityManager em;

	@Override
	public UserEntity create(UserEntity user) throws SQLDataException {

		try {
			em.persist(user);
			em.getTransaction().commit();
		} catch (Exception e) {
			// TODO: handle exception like logging
			em.getTransaction().rollback();
		}

		return user;
	}

	@Override
	public UserEntity read(long id) throws ArgumentException {
		UserEntity result = null;

		if (id < 0) {
			throw new ArgumentException("Id must not be smaller than 0");
		}

		try {
			result = em.find(UserEntity.class, id);
			em.detach(result);
		} catch (Exception e) {
			// ChangeTracking
			// TODO: handle exception
		}

		return result;
	}

	@Override
	public UserEntity update(UserEntity entity) throws ArgumentException {
		UserEntity result = null;

		if (entity == null) {
			throw new ArgumentException("entity must not be null");
		}

		try {
			// check if the entity related to the id is existing
			result = em.find(UserEntity.class, entity.getId());

			if (result != null) {
				em.detach(result);
				entity.setId(result.getId());
				em.persist(entity);
				em.getTransaction().commit();
			} else {
				// throw exception if no entity can be found with the specified id
				throw new SQLDataException(
						String.format("There is no entity with id %s", String.valueOf(entity.getId())));
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return result;
	}

	@Override
	public boolean delete(long id) throws ArgumentException {
		boolean result = false;

		if (id < 0) {
			throw new ArgumentException("Id must not smaller than 0");
		}

		try {

			UserEntity entity = em.find(UserEntity.class, id);
			if (entity != null) {
				em.remove(entity);
				result = true;
			}
		} catch (Exception e) {
			// ChangeTracking
			// TODO: handle exception
		}

		return result;
	}

	@Override
	public boolean delete(UserEntity userEntity) throws ArgumentException {
		boolean result = false;

		if (userEntity != null) {
			throw new ArgumentException("UserEntity must not null");
		}

		try {
			em.remove(userEntity);
			result = true;
		} catch (Exception e) {
			// ChangeTracking
			// TODO: handle exception
		}

		return result;
	}

	@Override
	public UserEntity createOrUpdate(UserEntity userEntity) throws Exception {
		UserEntity result = null;

		if (userEntity != null && userEntity.getId() != 0) {
			result = update(userEntity);
		} else {
			result = create(userEntity);
		}

		return result;
	}

}
