package com.tech11.UserManagement.data.api;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import com.tech11.UserManagement.data.models.CredentialsEntity;

@Transactional
public class CredentialsDataProvider implements DataProvider<CredentialsEntity> {

	@PersistenceContext(name = "h2ds")
	private EntityManager em;

	@Override
	public CredentialsEntity create()
	{
		// TODO Auto-generated method stub
		return createInternal();
	}

	private CredentialsEntity createInternal() {
		return null;
	}

	@Override
	public CredentialsEntity read() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CredentialsEntity update() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean delete() {
		// TODO Auto-generated method stub
		return false;
	}

}
