package com.tech11.UserManagement;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 */
@ApplicationPath("/data")
public class UserManagementRestApplication extends Application {
}
